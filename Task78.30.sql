--Test 1
SELECT * FROM subjects;
--Test 2
SELECT sj.subject_code,sj.subject_name,sj.credit, CONCAT(st.first_name,' ',st.last_name) as student_name
FROM subjects sj 
JOIN grades g ON sj.id = g.subject_id
JOIN students st ON g.student_id = st.id
LIMIT 3;
--Test 3
SELECT *
FROM subjects sj 
ORDER BY sj.credit ASC
LIMIT 4;
--Test 4
SELECT *
FROM students st
WHERE st.student_code = 20101234;
--Test 5
SELECT sj.subject_code,sj.subject_name,CONCAT(st.first_name, ' ', st.last_name) as student_name, g.grade,g.exam_date
FROM grades g
JOIN subjects sj ON g.subject_id = sj.id
JOIN students st ON g.student_id = st.id
WHERE sj.subject_code = 'MAT101';
--Test 6
SELECT sj.subject_code,sj.subject_name,sj.credit, g.exam_date
FROM subjects sj
JOIN grades g ON sj.id = g.subject_id
WHERE g.exam_date = '2021/05/04'
GROUP BY sj.subject_code;
--Test 7
SELECT sj.subject_name,CONCAT(st.first_name, ' ',st.last_name) as fullname, g.grade,g.exam_date 
FROM grades g 
JOIN students st ON g.student_id = st.id
JOIN subjects sj ON g.subject_id = sj.id
ORDER BY g.grade ASC
LIMIT 3;
--Test 8
SELECT sj.subject_code,sj.subject_name,CONCAT(st.first_name, ' ',st.last_name) as fullname, g.grade,g.exam_date 
FROM grades g 
JOIN students st ON g.student_id = st.id
JOIN subjects sj ON g.subject_id = sj.id
WHERE sj.subject_code = 'ECO101'
ORDER BY g.grade DESC
LIMIT 1;
